# Lexicon Digital Coding Challenge #
The task is to build a movie price comparison application. It can be a web application or a mobile app. To support the task, there are 4 endpoints available to get movie list and their price from 2 providers. It should be noted that these endpoints are **NOT reliable**. The solution must be able to handle this limitation gracefully.
For this challenge, I choose to develop an iOS application, written in **Swift**. I developed the solution in **XCode 11.5, targetting iOS 13.5**. The simulator that I tried it on is **iPhone 11**.
## Assumptions ##
To keep things simple, there are a number of assumptions that I made.

+ Each provider has the same number of movies

+ The movie code consist of **<provider code><movie code>**. The <movie code> part is the same for all providers. The only difference is the provider code. For example, if movie A has code 123456 and provider 1 has code xy and provider 2 has code vw, then movie A from provider 1 has code `xy123456` while the same movie from provider B has code `vw123456`

+ For demo purpose, to keep thing simple, I do not build a log in screen. However, I create a class that contains all data that can be configured (eg. available providers, default number of retry, API key). In the real usage, these values should be fetched from an API. See below in **Configurable** section for more details.

## Solution ##
### Dealing with unreliable endpoint ###
Based on my observation, the endpoints fail to return the expected result at the first attempt on numerous occassions. Subsequent call most of the time returns the expected result. Sometimes we need to re-try one more time to get the expected result. So, to solve this issue, I implement auto retry task in class RetryTask`. With this the complexity of retry is hidden in this implemetation. The following snippet shows the retry function.


    func retry<T, S>(_ attempts: Int, task: @escaping (_ completion:@escaping (Result<T, S>) -> Void) -> Void,  completion:@escaping (Result<T, S>) -> Void)
	{
		task({ result in
			switch result
			{
				case .success(_):
					completion(result)case .failure(let error):
					if attempts > 1
					{
						self.retry(attempts - 1, task: task, completion: completion)
					}
					else
					{
						completion(result)
					}
			}
		})
	}
I add an automatic test to demonstrate the functionality of this class. The `APIManager` class implementation also demonstrate this functionality. When the API call fails, it simply re-try the call again. For this task, I set the retry number to 5. If it still fails after 5 retry attempts, then the call fail.

### Data Structure ###
All data structure that I use is defined in file **DataStructure.swift**. Struct `MoviesData`, `Movie`, and `MovieDetail` are defined based on the structure of the returned JSON of the API calls.

+ Struct `Provider` is used to model the provider detail.

+ Struct `PrinceTheatreMoviePrice` is used to model the price that a movie provider charges for a movie.

+ Class `PrinceTheatreMovie` represent a Prince Theatre Movie. It contains the details of the movie (eg. title, synopsis, duration). It also contains a list of available providers and the price they charge.

### Configurable ###
The design that I create allows us to have dynamic providers. So, the app can deal with more than 2 providers. Class `Configurations` shows how I achieve this. In the real implementation, the values in this class shall be populated by the result of an API call. This will give us flexibility to differentiate available providers for different user. For example, John can only watch movie from provider A and B while Mary can watch from provider A, B and C. The data structure that I design is able to handle this. We only need to populate provider details correctly.
The value of API key should alse be returned from an API call. The data structure that I design is able to handle this. However, for demo purpose, I just hardcode it to keep thing simple.
The same thing applies to the default number of retry. This can be configured. Like the other two, the data structure that I design is able to handle this. For demo purpose, I hardcode it to 5.

### Data Readines ###
It is very important to only attempt to display the data once it is ready. To achieve this, I use Notification. Insidie `DataProvider` class, I post 2 notifications, `DataListReadyNotification`  and  `MovieDetailReadyNotification`. These notifications are posted when we have received the data successfully. There are 2 view controllers subscribe to these notifications, the main view controller and the movie detail view controller. They will start populating the UI elements once these notifications are received. Since the notification is posted on NON UI Thread, any attempt to update UI component must be done inside `DispatchQueue.main.async`.

    DispatchQueue.main.async {
    ...
    ...
    ...
    }

### Presentation ###
I present the movie list in a collection view. For the movie details, I decide to also display some extra information, such as synopsis, director, actors, release date. The user can see the price from all available providers, sorted by the lowest price.

## Tests ##
The unit tests that I add are only for the classes/functionality that I develop, the `RetryTask` and populating the data.
