//
//  MovieDetailViewController.swift
//  PrincesTheatre
//
//  Created by Rusmin Susanto on 26/1/21.
//  Copyright © 2021 Rusmin Susanto. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var movieTitle: UILabel!
	@IBOutlet weak var movieSynopsis: UILabel!
	@IBOutlet weak var movieDuration: UILabel!
	@IBOutlet weak var movieDirector: UILabel!
	@IBOutlet weak var movieActor: UILabel!
	@IBOutlet weak var movieGenre: UILabel!
	@IBOutlet weak var posterImage: UIImageView!

	var movieId: String!
	var posterUrl: String!
	private var priceDetails : [PrinceTheatreMoviePrice] = []

	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		tableView.register(UINib(nibName: "MoviePriceTableViewCell", bundle: nil),
						   forCellReuseIdentifier: "MoviePriceTableViewCell")

		// Tells iOS not to draw empty row separator
		tableView.tableFooterView = UIView()

		// Fetch image
		let imageUrl = URL(string: self.posterUrl!)!

		DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: imageUrl) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
						self!.posterImage.image = image
                    }
                }
            }
        }

		let movieDetail = DataProvider.shared.getMovieDetail(id: self.movieId)

		// If all details are available, just use it. Don't fetch anymore
		if movieDetail.detailIsComplete()
		{
			handleDataReady()
		}
		else
		{
			// Register for notification
			let nc = NotificationCenter.default
			nc.addObserver(self, selector: #selector(handleDataReady),
				name: Notification.Name(DataProvider.shared.MovieDetailReadyNotification),
				object: DataProvider.shared)

			// Request for fetching movie detail
			DataProvider.shared.fetchMovieDetail(id: self.movieId)
		}
    }

	deinit {
		NotificationCenter.default.removeObserver(self)
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.priceDetails.count
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let priceDetail = self.priceDetails[indexPath.row]

		let alert = UIAlertController(title: "You are going to purchase a movie for " +
			String(format: "$%.2f", priceDetail.price!),
			message: "This action cannot be undone. Are you sure?", preferredStyle: .alert)

		alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
		alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))

		self.present(alert, animated: true)
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "MoviePriceTableViewCell") as! MoviePriceTableViewCell
		let priceDetail = self.priceDetails[indexPath.row]

		cell.cinemaName.text = Configurations.shared.providerName(id: priceDetail.providerId ?? "???")
		cell.moviePrice.text = String(format: "$ %.2f", priceDetail.price!)

		return cell
	}

	@objc func handleDataReady()
	{
		let movieDetail = DataProvider.shared.getMovieDetail(id: self.movieId)
		priceDetails = Array(movieDetail.providers.values)

		// Sort by price
		priceDetails.sort { (a, b) -> Bool in
			return a.price! < b.price!
		}

		DispatchQueue.main.async {
			self.movieTitle.text = movieDetail.title
			self.movieSynopsis.text = movieDetail.synopsis
			self.movieDuration.text = String("Duration : " + movieDetail.runTime)
			self.movieDirector.text = String("Director: " + movieDetail.director)
			self.movieActor.text = String("Actors : " + movieDetail.actors)
			self.movieGenre.text = String("Released : " + movieDetail.released)
			self.tableView.reloadData()
		}
	}
}
