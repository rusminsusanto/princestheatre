//
//  MoviePriceTableViewCell.swift
//  LexiconDigital
//
//  Created by Rusmin Susanto on 25/1/21.
//  Copyright © 2021 Rusmin Susanto. All rights reserved.
//

import UIKit

class MoviePriceTableViewCell: UITableViewCell {

	@IBOutlet weak var cinemaName: UILabel!
	@IBOutlet weak var moviePrice: UILabel!

	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
