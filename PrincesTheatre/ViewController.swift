//
//  ViewController.swift
//  PrincesTheatre
//
//  Created by Rusmin Susanto on 26/1/21.
//  Copyright © 2021 Rusmin Susanto. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

	@IBOutlet weak var collectionView: UICollectionView!

	private var moviesArray: [PrinceTheatreMovie] = []

	override func viewDidLoad() {
		super.viewDidLoad()

		// Register for notification
		let nc = NotificationCenter.default
		nc.addObserver(self, selector: #selector(handleDataReady),
			name: Notification.Name(DataProvider.shared.DataListReadyNotification),
			object: DataProvider.shared)

		// Fetch movie list
		DataProvider.shared.fetchMovieList()

		// Do any additional setup after loading the view.
		collectionView.register(UINib(nibName: "CustomCollectionViewCell", bundle: nil),
			forCellWithReuseIdentifier: "CustomCollectionViewCell")
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}

	@IBAction func show(_ sender: Any) {
		let movieDetail = MovieDetailViewController()

		self.navigationController?.pushViewController(movieDetail, animated: true)
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.moviesArray.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell",
					for: indexPath) as! CustomCollectionViewCell

		cell.navigationController = self.navigationController

		// Set up properties
		let movie = moviesArray[indexPath.row]
		let imageUrl = URL(string: movie.imageURL!)!

		cell.movieTitle.text = movie.title
		cell.movieId = movie.id
		cell.posterUrl = movie.imageURL

		DispatchQueue.global().async { [cell] in
            if let data = try? Data(contentsOf: imageUrl) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
						cell.moviePoster.image = image
                    }
                }
            }
        }

		return cell
	}

	@objc func handleDataReady()
	{
		self.moviesArray = Array(DataProvider.shared.getMovieList().values)

		// Reload data on main thread.
		DispatchQueue.main.async {
			self.collectionView.reloadData()
		}
	}
}

