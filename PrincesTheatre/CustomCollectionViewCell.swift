//
//  CustomCollectionViewCell.swift
//  LexiconDigital
//
//  Created by Rusmin Susanto on 25/1/21.
//  Copyright © 2021 Rusmin Susanto. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var moviePoster: UIImageView!
	@IBOutlet weak var movieTitle: UILabel!

	var movieId: String!
	var posterUrl: String!

	var navigationController: UINavigationController!

	override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
		let tap = UITapGestureRecognizer(target: self, action: #selector(CustomCollectionViewCell.imageTapped))
			moviePoster.addGestureRecognizer(tap)

		moviePoster.isUserInteractionEnabled = true
	}

	@objc func imageTapped()
	{
		let movieDetail = MovieDetailViewController()

		movieDetail.movieId = self.movieId
		movieDetail.posterUrl = self.posterUrl
		
		self.navigationController?.pushViewController(movieDetail, animated: true)
	}
}
