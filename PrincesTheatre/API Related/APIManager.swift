//
//  RetriableAPIManager.swift
//  LexiconDigital
//
//  Created by Rusmin Susanto on 25/1/21.
//  Copyright © 2021 Rusmin Susanto. All rights reserved.
//

import Foundation

enum APIError: Error {
	case error4xx
	case error5xx
	case errorUnknown
}

typealias MoviesSuccessHandler = (_ success:MoviesData) -> Void
typealias MovieDetailSuccessHandler = (_ success:MovieDetail) -> Void
typealias ErrorHandler = (_ error:APIError) -> Void

class APIManager : RetryTask
{
	private let rest = RestManager()

	static let shared = APIManager()

	// Ensure noone can create this class to enforce singleton.
	private override init()
	{
	}

	private func fetchMovies(url: URL, completionHandler: @escaping (Result<MoviesData, APIError>) -> Void)  {
		rest.makeRequest(toURL: url, withHttpMethod: .get, withApiKey: Configurations.shared.apiKey()) { (results) in
			if  results.response?.httpStatusCode != 200 {
				let errorCategory = results.response!.httpStatusCode / 100
				print(results.response!.httpStatusCode)
				if errorCategory == 4
				{
					completionHandler(.failure(.error4xx))
				}
				else if errorCategory == 5
				{
					completionHandler(.failure(.error5xx))
				}
				else
				{
					completionHandler(.failure(.errorUnknown))
				}
				return
			}

			if let data = results.data {
				let decoder = JSONDecoder()
				decoder.keyDecodingStrategy = .convertFromSnakeCase

				guard let moviesData = try? decoder.decode(MoviesData.self, from: data) else
				{
					completionHandler(.failure(.errorUnknown))
					return
				}

				completionHandler(.success(moviesData))
			}
		}
	}

	private func fetchMovieDetail(url: URL, completionHandler: @escaping (Result<MovieDetail, APIError>) -> Void)  {
		rest.makeRequest(toURL: url, withHttpMethod: .get, withApiKey: Configurations.shared.apiKey()) { (results) in
			if  results.response?.httpStatusCode != 200 {
				print(results.response!.httpStatusCode)
				let errorCategory = results.response!.httpStatusCode / 100
				if errorCategory == 4
				{
					completionHandler(.failure(.error4xx))
				}
				else if errorCategory == 5
				{
					completionHandler(.failure(.error5xx))
				}
				else
				{
					completionHandler(.failure(.errorUnknown))
				}
				return
			}

			if let data = results.data {
				let decoder = JSONDecoder()
				decoder.keyDecodingStrategy = .convertFromSnakeCase

				guard let movieDetail = try? decoder.decode(MovieDetail.self, from: data) else
				{
					completionHandler(.failure(.errorUnknown))
					return
				}

				completionHandler(.success(movieDetail))
			}
		}
	}

	func fetchMoviesWithRetry(url:String, successHandler:@escaping (MoviesSuccessHandler), errorHandler:@escaping (ErrorHandler))
	{
		retry(Configurations.shared.defaultRetryCount(), task: { (result) in
			self.fetchMovies(url: URL(string:url)!, completionHandler: result)
		}) { (newResult) in
			switch newResult
			{
				case .success(let movieData):
					successHandler(movieData)
				case .failure(let error):
					errorHandler(error)
			}
		}
	}

	func fetchMovieDetailWithRetry(url: String, movieId: String, successHandler:@escaping (MovieDetailSuccessHandler), errorHandler:@escaping (ErrorHandler))
	{
		retry(Configurations.shared.defaultRetryCount(), task: { (result) in
			self.fetchMovieDetail(url: URL(string:url+movieId)!, completionHandler: result)
		}) { (newResult) in
			switch newResult
			{
				case .success(let movieDetail):
					successHandler(movieDetail)
				case .failure(let error):
					errorHandler(error)
			}
		}
	}
}
