//
//  RetryTask.swift
//  RestAPITest
//
//  Created by Rusmin Susanto on 24/1/21.
//  Copyright © 2021 Rusmin Susanto. All rights reserved.
//

import Foundation

class RetryTask
{
	internal func retry<T, S>(_ attempts: Int, task: @escaping (_ completion:@escaping (Result<T, S>) -> Void) -> Void,  completion:@escaping (Result<T, S>) -> Void)
	{
		task({ result in
			switch result
			{
				case .success(_):
					completion(result)

				case .failure(let error):
					print("RETRIES left \(attempts) and error = \(error)")
					if attempts > 1
					{
						self.retry(attempts - 1, task: task, completion: completion)
					}
					else
					{
						completion(result)
					}
			}
		})
	}
}
