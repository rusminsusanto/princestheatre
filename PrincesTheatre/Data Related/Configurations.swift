//
//  Providers.swift
//  PrincesTheatre
//
//  Created by Rusmin Susanto on 26/1/21.
//  Copyright © 2021 Rusmin Susanto. All rights reserved.
//

import Foundation

class Configurations
{
	static let shared = Configurations()

	// The contents of this class can be configured via API. For example, as part of log in API
	// returned value, we can also return the content of this class.
	// - Eg. user A is only authorised to select from 2 providers or user B is only authorised to
	//   select from 3 providers
	// - We also return the number of default retry count
	// - Also return the API key
	// For demo purpose, I simply hardcode it
	private var providerDetails : [Provider] = [
		Provider(id: "cw",
				 name: "Cinema World",
				 movieListUrlString: "https://challenge.lexicondigital.com.au/api/cinemaworld/movies",
				 movieDetailUrlString: "https://challenge.lexicondigital.com.au/api/cinemaworld/movie/"),
		Provider(id: "fw",
				 name: "Film World",
				 movieListUrlString: "https://challenge.lexicondigital.com.au/api/filmworld/movies",
				 movieDetailUrlString: "https://challenge.lexicondigital.com.au/api/filmworld/movie/")
	]
	
	private init()
	{
	}

	// In real implementation, this should be returned from API (eg. login api).
	// For demo purpose it's ok.
	func apiKey() -> String
	{
		return "Yr2636E6BTD3UCdleMkf7UEdqKnd9n361TQL9An7"
	}

	// This can also be returned from configuration that is returned from an API call
	func defaultRetryCount() -> Int
	{
		return 5
	}
	
	func providers() -> [Provider]
	{
		return providerDetails
	}

	func providerName(id: String) -> String
	{
		let provider = providerDetails.filter { (p) -> Bool in
			return p.id == id
		}

		return provider.first?.name ?? "??"
	}
}
