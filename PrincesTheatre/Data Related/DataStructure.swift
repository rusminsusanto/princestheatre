import Foundation

var defaultString = String(stringLiteral: "")
var defaultDouble = 0.0

struct MoviesData: Codable, CustomStringConvertible {
    var Provider: String?
    var Movies: [Movie]?

    var description: String {
        var desc = """
        Provider = \(Provider ?? defaultString)
        Movies:

        """
        if let movies = Movies {
            for movie in movies {
                desc += movie.description
            }
        }

        return desc
    }
}

struct Movie: Codable, CustomStringConvertible {
    var ID: String?
    var Title: String?
	// Type member must not be named 'Type', since it would conflict with the 'foo.Type' expression
	// So, that's why I change it to `Type`
	var `Type`: String?
    var Poster: String?

	init(ID: String, Title: String, Poster: String)
	{
		self.ID  = ID
		self.Title = Title
		self.Type = "movie"
		self.Poster = Poster
	}

	var description: String {
        return """
        ------------
        ID = \(ID ?? defaultString)
        Title = \(Title ?? defaultString)
        Type = \(Type ?? defaultString)
        Poster = \(Poster ?? defaultString)
        ------------
        """
    }
}

struct MovieDetail: Codable, CustomStringConvertible {
    var ID: String?
    var Title: String?
    var Year: String?
    var Rated: String?
    var Released: String?
    var Runtime: String?
    var Genre: String?
    var Director: String?
    var Writer: String?
    var Actors: String?
    var Plot: String?
    var Language: String?
    var Country: String?
    var Poster: String?
	// Type member must not be named 'Type', since it would conflict with the 'foo.Type' expression
	// So, that's why I change it to `Type`
    var `Type`: String?
    var Production: String?
    var Price: Double?

	var description: String {
		return """
		------------
		ID = \(ID ?? defaultString)
		Title = \(Title ?? defaultString)
		Year = \(Year ?? defaultString)
		Rated = \(Rated ?? defaultString)
		Released = \(Released ?? defaultString)
		Runtime = \(Runtime ?? defaultString)
		Genre = \(Genre ?? defaultString)
		Director = \(Director ?? defaultString)
		Writer = \(Writer ?? defaultString)
		Actors = \(Actors ?? defaultString)
		Plot = \(Plot ?? defaultString)
		Language = \(Language ?? defaultString)
		Country = \(Country ?? defaultString)
		Poster = \(Poster ?? defaultString)
		Type = \(Type ?? defaultString)
		Production = \(Production ?? defaultString)
		Price = \(Price ?? defaultDouble)
		------------
		"""
    }
}

struct Provider
{
	var id: String!
	var name: String!
	var movieListUrlString: String!
	var movieDetailUrlString: String!
}

struct PrinceTheatreMoviePrice
{
	var providerId: String?
	var price: Double?
	init(providerId: String?, price: Double?)
	{
		self.providerId = providerId
		self.price = price
	}
}

class PrinceTheatreMovie
{
	var id: String?	// Id here doesn't contain provider prefix
	var imageURL: String?
	var title: String?
	var synopsis: String = ""
	var released: String = ""
	var runTime: String = ""
	var director: String = ""
	var actors: String = ""
	var language: String = ""

	var providers: [String:PrinceTheatreMoviePrice] = [:]

	private let semaphore = DispatchSemaphore(value: 1)
	private var detailsIsComplete = false

	init(id: String, imageURL: String, title: String)
	{
		self.id = id
		self.imageURL = imageURL
		self.title = title
	}

	func setDetail(complete:Bool)
	{
		self.detailsIsComplete = complete
	}

	func detailIsComplete() -> Bool
	{
		return self.detailsIsComplete
	}

	func addProvider(provider: String, price: Double)
	{
		// Add protection as this function can be called by more than 1 thread at the same time.
		semaphore.wait()

		if !self.providers.contains(where: { (key, value) -> Bool in return key == provider })
		{
			self.providers[provider] = PrinceTheatreMoviePrice(providerId:provider, price:price)
		}

		semaphore.signal()
	}
}
