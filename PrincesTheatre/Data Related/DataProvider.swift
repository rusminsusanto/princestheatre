//
//  DataProvider.swift
//  PrincesTheatre
//
//  Created by Rusmin Susanto on 26/1/21.
//  Copyright © 2021 Rusmin Susanto. All rights reserved.
//

import Foundation

class DataProvider
{
	static let shared = DataProvider()

	let DataListReadyNotification = "DataListReadyNotification"
	let MovieDetailReadyNotification = "MovieDetailReadyNotification"

	private var availableMovies: [String:PrinceTheatreMovie] = [:]
	private let providerCount = Configurations.shared.providers().count
	private var fetchCounter = 0

	private let semaphore = DispatchSemaphore(value: 1)

	private init()
	{
	}

	func fetchMovieList()
	{
		let providers = Configurations.shared.providers()
		self.fetchCounter = 0

		for p in providers
		{
			APIManager.shared.fetchMoviesWithRetry(url: p.movieListUrlString,
				successHandler: { [weak self](movies) in
					self?.addMovieList(movies: movies.Movies ?? [])
					self?.fetchCounter += 1
					if self?.fetchCounter == self?.providerCount
					{
						self?.postMovieListReady()
					}
				},
				errorHandler: { (error) in
					print("Error fetching movie list: \(error)")
				})
		}
	}

	func fetchMovieDetail(id: String)
	{
		let providers = Configurations.shared.providers()
		self.fetchCounter = 0

		for p in providers
		{
			let movieId = p.id + id
			APIManager.shared.fetchMovieDetailWithRetry(url: p.movieDetailUrlString, movieId: movieId,
				successHandler: { [weak self](movieDetail) in
					self?.addMoviePrice(movieId: id, providerId:p.id, movieDetail: movieDetail)
					self?.fetchCounter += 1
					if self?.fetchCounter == self?.providerCount
					{
						let movie = self?.availableMovies[id]!
						movie?.setDetail(complete: true)
						self?.postMovieDetailReady()
					}
				},
				errorHandler: { (error) in
					print("Error : \(error)")
				})
		}
	}

	func addMovieList(movies:[Movie])
	{
		for movie in movies
		{
			// Remove the first 2 characters
			let newId = String(movie.ID?.dropFirst(2) ?? "xxxx")

			// Add protection as this function can be called by more than 1 thread at the same time.
			semaphore.wait()

			if !self.availableMovies.contains(where: { (key, value) -> Bool in return key == newId })
			{
				self.availableMovies[newId] = PrinceTheatreMovie(id: newId,
					imageURL: movie.Poster ?? "???", title: movie.Title ?? "???")
			}

			semaphore.signal()
		}
	}

	func addMoviePrice(movieId:String, providerId:String, movieDetail:MovieDetail)
	{
		let movie = self.availableMovies[movieId]!

		movie.synopsis = movieDetail.Plot ?? ""
		movie.released = movieDetail.Released ?? ""
		movie.runTime = movieDetail.Runtime ?? ""
		movie.director = movieDetail.Director ?? ""
		movie.actors = movieDetail.Actors ?? ""
		movie.language = movieDetail.Language ?? ""

		movie.addProvider(provider: providerId, price: movieDetail.Price ?? 0.0)
	}

	private func postMovieListReady()
	{
		let nc = NotificationCenter.default
		nc.post(name: Notification.Name(self.DataListReadyNotification), object: self)
	}

	private func postMovieDetailReady()
	{
		let nc = NotificationCenter.default
		nc.post(name: Notification.Name(self.MovieDetailReadyNotification), object: self)
	}

	func getMovieList() -> [String:PrinceTheatreMovie]
	{
		return availableMovies
	}

	func getMovieDetail(id: String) -> PrinceTheatreMovie
	{
		return availableMovies[id]!
	}
}
