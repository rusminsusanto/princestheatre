//
//  RetriableTaskTest.swift
//  LexiconDigitalTests
//
//  Created by Rusmin Susanto on 25/1/21.
//  Copyright © 2021 Rusmin Susanto. All rights reserved.
//

import Foundation

enum MyError: Error {
	case timeOut
	case errorUnknown
}

typealias TestSuccessHandler = (_ success:Int) -> Void
typealias TestErrorHandler = (_ error:MyError) -> Void

class RetriableTaskTest : RetryTask
{
	var attemptCount = 0

	private func doSomething(count:Int, completionHandler: @escaping (Result<Int, MyError>) -> Void)
	{
		attemptCount += 1

		guard attemptCount > count else
		{
			completionHandler(.failure(.timeOut))
			return
		}

		completionHandler(.success(200))
	}

	private func doSomethingThatFail(count:Int, completionHandler: @escaping (Result<Int, MyError>) -> Void)
	{
		attemptCount += 1

		guard attemptCount > 100 else
		{
			completionHandler(.failure(.errorUnknown))
			return
		}

		completionHandler(.success(200))
	}

	func executeTask(successHandler:@escaping (TestSuccessHandler), errorHandler:@escaping (TestErrorHandler))
	{
		retry(3, task: { (result) in
			self.doSomething(count: 2, completionHandler: result)
		}) { (newResult) in
			switch newResult {
			case .success(_):
				successHandler(200)
			case .failure(let error):
				errorHandler(error)
			}
		  }
	}

	func executeFailingTask(successHandler:@escaping (TestSuccessHandler), errorHandler:@escaping (TestErrorHandler))
	{
		retry(3, task: { (result) in
			self.doSomethingThatFail(count: 2, completionHandler: result)
		}) { (newResult) in
			switch newResult {
			case .success(_):
				successHandler(200)
			case .failure(let error):
				errorHandler(error)
			}
		  }
	}
}
