//
//  PrincesTheatreTests.swift
//  PrincesTheatreTests
//
//  Created by Rusmin Susanto on 26/1/21.
//  Copyright © 2021 Rusmin Susanto. All rights reserved.
//

import XCTest
@testable import PrincesTheatre

class PrincesTheatreTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

		let cwMovie1 = Movie(ID: "cw3748528", Title: "Rogue One: A Star Wars Story", Poster: "https://images1.jpg")
		let cwMovie2 = Movie(ID: "cw3778644", Title: "Solo: A Star Wars Story", Poster: "https://images2.jpg")
		var cwMoviesData = MoviesData()
		cwMoviesData.Movies = [cwMovie1, cwMovie2]

		DataProvider.shared.addMovieList(movies: cwMoviesData.Movies!)

		let fwMovie1 = Movie(ID: "fw3748528", Title: "Rogue One: A Star Wars Story", Poster: "https://images1.jpg")
		let fwMovie2 = Movie(ID: "fw3778644", Title: "Solo: A Star Wars Story", Poster: "https://images2.jpg")
		var fwMoviesData = MoviesData()
		fwMoviesData.Movies = [fwMovie1, fwMovie2]

		DataProvider.shared.addMovieList(movies: fwMoviesData.Movies!)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDataProviderMovies() throws {
        // Test our data provider to ensure it has the data in the expected format. It should end up with
		// 2 movies, rather than 4
		XCTAssertEqual(2, DataProvider.shared.getMovieList().count, "Error! There should be 2 available movies")

		let movie1 = DataProvider.shared.getMovieList()["3778644"]

		XCTAssertEqual("Solo: A Star Wars Story", movie1?.title, "Error! Mismatch title")

		let movie2 = DataProvider.shared.getMovieList()["3748528"]

		XCTAssertEqual("Rogue One: A Star Wars Story", movie2?.title, "Error! Mismatch title")
    }

	func testDataManagerAddMoviePrice() throws {
		// Test the data manager when adding price
		let dataProvider = DataProvider.shared

		var movie1CW = MovieDetail()
		movie1CW.ID = "cw3748528"
		movie1CW.Price = 24.9

		var movie1FW = MovieDetail()
		movie1FW.ID = "fw3748528"
		movie1FW.Price = 24.0

		var movie2CW = MovieDetail()
		movie2CW.ID = "cw3778644"
		movie2CW.Price = 22.9

		var movie2FW = MovieDetail()
		movie2FW.ID = "fw3778644"
		movie2FW.Price = 23.4

		dataProvider.addMoviePrice(movieId: "3748528", providerId: "cw", movieDetail: movie1CW)
		dataProvider.addMoviePrice(movieId: "3778644", providerId: "cw", movieDetail: movie2CW)

		dataProvider.addMoviePrice(movieId: "3748528", providerId: "fw", movieDetail: movie1FW)
		dataProvider.addMoviePrice(movieId: "3778644", providerId: "fw", movieDetail: movie2FW)

		let movie1 = dataProvider.getMovieList()["3748528"]

		let movie2 = dataProvider.getMovieList()["3778644"]

		XCTAssertEqual(2, movie1?.providers.count, "Error! There should be 2 providers");
		XCTAssertEqual(2, movie2?.providers.count, "Error! There should be 2 providers");
	}

	func testRetriableTask() throws {
		// Test the retriable task. It will eventually successful
		let task = RetriableTaskTest()
		var actualResult = 0

		task.executeTask(successHandler: { (result) in
			// Actual result is set to 200 here
			actualResult = result
		},
		errorHandler: { (error) in
			print(error)
		})

		XCTAssertEqual(200, actualResult, "Error, actual result should be 200")
		XCTAssertEqual(3, task.attemptCount, "Error, the attempt count should be 2")
	}

	func testRetriableFailedTask() throws {
		// Test the retriable task. It will eventually failed
		let task = RetriableTaskTest()
		var myError : MyError = .timeOut

		task.executeFailingTask(successHandler: { (result) in
			// Do nothing
		},
		errorHandler: { (error) in
			myError = error
		})

		XCTAssertEqual(MyError.errorUnknown, myError, "Error, the error should be errorUnknown")
		XCTAssertEqual(3, task.attemptCount, "Error, the attempt count should be 2")
	}

}
